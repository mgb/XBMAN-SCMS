#欢迎使用XBMAN-SCMS
XBMAN-SCMS 是基于ansible开发的配置文件管理、下发的工具，基于ssh协议来管理，客户端无需安装agent。
支持常见系统:CentOS, RedHat, Fedora, Amazon Linux

###截图：
登陆

![登陆](http://git.oschina.net/weihaoxuan/images/raw/master/xbman_scms/login.jpg "登陆")

首页

![首页](http://git.oschina.net/weihaoxuan/images/raw/master/xbman_scms/index.jpg "首页")

命令执行

![命令执行](http://git.oschina.net/weihaoxuan/images/raw/master/xbman_scms/cmd.jpg "命令执行")

任务列表

![任务列表](http://git.oschina.net/weihaoxuan/images/raw/master/xbman_scms/task.jpg "任务列表")

部署方法：
    执行 install.sh